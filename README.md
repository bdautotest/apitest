

- запуск рабочих тестов
   
    mvn install -DsuiteXml=testng_J15_british.xml
    mvn install -DsuiteXml=testng_J15_scottish.xml
    mvn install -DsuiteXml=testng_J3_british.xml
    mvn install -DsuiteXml=testng_J3_scottish.xml

mvn clean install -DsuiteXml=testng_J15.xml

запуск  только с тестов с тегами 
    mvn install -DsuiteXml=testng_J15.xml -Dcucumber.options="--tags @runThis --tags @AlfaTest "
    mvn install -Dcucumber.options="--tags @runThis"

вывод только сценариев по сайту
     mvn -DsuiteXml=testng_J13.xml -Dcucumber.options="-d "
вывод только сценариев по сайту для определеного тега или наборов тегов
     mvn -DsuiteXml=testng_J13.xml -Dcucumber.options="-d --tags @runThis"
 
@AlfaTest - в процесе разработки
@FunctionTests - рабочие всего всего функционала
@SmokeTest - быстрый тест основного функционала
@api
пока нету
@british 
@scottish

@_01_System 
@_02_Manage
@_03_The_Setting
@_04_Daily 
@_05_Children    
@_06_Parents  
@_07_Staff
@_08_Calendar  
@_09_Progress - в разработке
@_10_Planning 
@_11_Medical
@_12_Register
@_13_Accounts 
@_14_Legal
@_15_Galleries

отчеты находыться в target

для jenkins
cucumberj15.json
cucumberj3.json

отчеты огурца в HTML виде 
target/j15/
target/j3/

отчеты TestNG
target/surefire-reports

cucumber-html-reports
