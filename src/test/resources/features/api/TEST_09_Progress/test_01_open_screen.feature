@FunctionTests
@API
@J3
@J15
@_09_Progress 
@_09_Progress_date_achieved
@british
@scottish

Feature: open the progress section
request rooms and children's lists
открыть секцию прогресса и запросить комнаты и списки детей

  @J3
  @J15 
   Scenario: Get a list of available rooms for progress.
      получить список детей для секции прогресс
      option=com_sted_mobile&controller=predefined_room&task=room&limitstart_=0
      Given Make sure there are rooms 
      When the status code is 200
      And response includes the following
         | result.msg 		| Action complete  |
         | result.complete 	| true             |

      And get list of all rooms    

 
   @BlockHooks
   Scenario: Get a list of children for the progress section
      option=com_sted_mobile&controller=newprogress&task=getChildList
      получить список детей для секции прогресс
      
      Given Make sure there are children for the progress section
      When the status code is 200
      And response includes the following
         | result.msg 		| Child list complete.  |
         | result.complete 	| true                  |
      When Get a list of children for the progress section

 
      
  @J3
  @J15  
   @BlockHooks
   Scenario: Get all the data on the progress Overview Learning Journey of the child
      ?option=com_sted_mobile&controller=newprogress&task=getNewprogress&type=cl&chid=
      We take the first child from the list and make inquiries for all type
      получить все данные о прогрессе ребенка
      берем первого ребенка из списка и делаем запросы по всем type

      Given make a request for "cl" type
      And there must be fields children FirstName for the progress is progress Overview
      And there must be fields children LastName for the progress is progress Overview
      And there must be fields children Age for the progress is progress Overview
      And there must be fields children DateOfBirth for the progress is progress Overview
      And there must be fields children AssignedRoom for the progress is progress Overview
   
  @J3
  @J15
@BlockHooks
   Scenario: Added achievement dates 
      Added achievement dates 
      Добавлены даты достижений
      ?option=com_sted_mobile&controller=newprogress&task=dateAchievedNewprogress=%@&action=1
      Given clear field if there is a date type "cl" tab "tab1" itemid "CLT10"
      When add a date achievements today type "cl" tab "tab1" itemid "CLT10"
      When the status code is 200
      And response includes the following
         | result.msg 		| date achieved add complete.  |
         | result.complete 	| true                         |
      Then make a request for "cl" type 
      Then check the date must "match" chievements today type "cl" tab "tab1" itemid "CLT10"
  @J3
  @J15
@BlockHooks
   Scenario: Delete the achievement date added earlier
       ?option=com_sted_mobile&controller=newprogress&task=dateAchievedNewprogress=%@&action=2
      Given delete a date achievements today type "cl" tab "tab1" itemid "CLT10"
      When the status code is 200
      And response includes the following
         | result.msg 		| date achieved delete complete.  |
         | result.complete 	| true                            |
      When make a request for "cl" type 
      Then check the date must "not_match" chievements today type "cl" tab "tab1" itemid "CLT10"
      