@FunctionTests
@API
@J3
@J15
@_09_Progress 
@_09_Progress_Next_Steps
@british
@scottish
@AlfaTest

Feature: Add and edit and delete Next Steps 
   добавлять, редактировать и удалять наблюдения

  
   Scenario: Add pending Next Steps
      Добавить наблюдение для первого ребенка из списка 
      ?option=com_sted_mobile&controller=newprogress&task=add_pending
      Given Make sure there are children for the progress section
      When the status code is 200
      And response includes the following
         | result.msg 		| Child list complete.  |
         | result.complete 	| true                  |
      And Get a list of children for the progress section
      Given add notes pending Next Steps of childrens Next Step Text "test" Date "17.04.2018"
      
         | childType| childTab| childItem| 
         | cl      | tab1  | CLT97  | 
         | cl      | tab1  | CLT1   | 

      When the status code is 200
      And response includes the following
         | result.msg 		| Action complete  |
         | result.complete 	| true             |
         | response     	| ok               |
           
      Given make a request for "cl" type 
      And check the parameters for the type progress section
         |  cl  | tab1 | CLT97 | NSStatus | 1 |
         |  cl  | tab1 | CLT1  | NSStatus | 1 |


   Scenario: Next steps - individual learning plans
           ?option=com_sted_mobile&controller=new_individual&task=individual
            проверить Next steps в - индивидуальные планы обучения

      Given  Shown below are the existing Children on the system
      When the status code is 200
      Then response includes the following
         | result.msg 		| Action complete  |
         | result.complete 	| true             |
      And check existing children for next steps in the system  


   Scenario: View selected children's next steps
      Given View selected children next steps

#    Scenario: Remove Next Steps for the child
#       Удалить ожидающие наблюдения для ребенка
#       ?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_del
#       Given make a request for "cl" type 
#       When delete for pending presence for the child
#          | childType|  childItem | text | dateObs   |
#          | cl       |  CLT97     | test | 17.04.2018|
#          | cl       |  CLT1      | test | 17.04.2018|
#       Then make a request for "cl" type 
#       And check the parameters for the type progress section
#          |  cl  | tab1 | CLT97 | NSStatus |  |
#          |  cl  | tab1 | CLT1  | NSStatus |  |
#  
