@FunctionTests
@API
@J3
@J15
@_09_Progress 
@_09_Progress_observation
@british
@scottish

Feature: Add and edit and delete observations 
   добавлять, редактировать и удалять наблюдения


   Scenario: Сlear all pending observations
      очистить все ожидающие наблюдения
      Given Make sure there are children for the progress section
      When the status code is 200
      And response includes the following
         | result.msg 		| Child list complete.  |
         | result.complete 	| true                  |
      And Get a list of children for the progress section
      And make a request for "cl" type   
      Then Сlear all notes pending observations 
         | cl | tab1  | CLT97  |
         | cl | tab1  | CLT1   |

     @BlockHooks

   Scenario: Add pending observations
      Добавить наблюдение для первого ребенка из списка 
      ?option=com_sted_mobile&controller=newprogress&task=add_pending
     
      Given notes pending observations of childrens 
      
         | chainType| chainTab| chainItem| text | dateObs   | completed | type |
         | cl-x      | cl-tab1  | CLT97  | test | 17.04.2018| false     | obs  |
         | cl-x      | cl-tab1  | CLT1   | test | 17.04.2018| true      | note |

      When make a request for "cl" type   
      Then check for "ObsNotes" presence for the child
         | chainType  | chainTab  | chainItem   | text | dateObs    |  title       | type |
         |  cl        | tab1      | CLT97       | test | 17.04.2018 | Observation: | obs  |
         |  cl        | tab1      | CLT1        | test | 17.04.2018 | Note:        | obs  |

   Scenario: Remove pending observations for the child
      Удалить ожидающие наблюдения для ребенка
      ?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_del
      Given make a request for "cl" type 
      When delete for pending presence for the child
         | chainType|  chainItem | text | dateObs   |
         | cl       |  CLT97     | test | 17.04.2018|
         | cl       |  CLT1      | test | 17.04.2018|
      Then make a request for "cl" type 
      And check the parameters for the type progress section
         |  cl  | tab1 | CLT97 | ObsNotes |  |
         |  cl  | tab1 | CLT1  | ObsNotes |  |
   Scenario: Edit pending observations
      Редактировать наблюдение для первого ребенка из списка 
      ?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_edit
     
      Given notes pending observations of childrens 
      
         | chainType| chainTab| chainItem| text | dateObs   | completed | type |
         | cl-x      | cl-tab1  | CLT97  | test | 17.04.2018| false     | obs  |
      
      When edit notes pending observations of childrens 
         | chainType| chainTab| chainItem| text      | textBack | dateObs   | dateObsBack | 
         | cl-x      | cl-tab1  | CLT97  | edit test | test     | 17.04.2018|  17.04.2018 |
       And response includes the following
         | result.complete 	| true                       |
      When make a request for "cl" type   
      Then check for "ObsNotes" presence for the child
         | chainType  | chainTab  | chainItem   | edit test | dateObs    |  title       | type |
         |  cl        | tab1      | CLT97       | edit test | 15.05.2018 | Observation: | obs  |
         Given make a request for "cl" type 
      When delete for pending presence for the child
         | chainType|  chainItem | text | dateObs   |
         | cl       |  CLT97     | edit test | 17.04.2018|
      
 
