@FunctionTests
@API
@J3
@J15
@_15_Galleries
@_15_1_Video
@british
@scottish

Feature: I go to the section video gallery
Open the video section and request rooms and children lists and server setups
открыть секцию video и запросить комнаты и списки детей и установки сервера


   Scenario: Get server system settings.
     получить установки системы сервера
      option=com_sted_mobile&controller=video_gallery&task=ajax_child_start_opt
      Given Got the installation of the system for video
      When the status code is 200
      And response includes the following
         | result.msg 		| Video options  |
         | result.complete 	| true           |

      Then check the rest of the video server settings   


   Scenario: Get a list of available children for the video section
      получить список доступных детей для видео секции
      ?option=com_sted_mobile&controller=video_gallery&task=ajax_main
      Given Returns list of children in video gallerie
      When the status code is 200
      And response includes the following
         | result.msg 		| get room true  |
         | result.complete 	| true           |

      Then check the answer of the childrens list for the video section

