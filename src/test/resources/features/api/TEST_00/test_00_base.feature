@FunctionTests
@API
@british
@scottish
@BlockHooks
@J3
@J15

Feature: Check site availability 
   check site availability
   проверить доступность сайта, если сайт не доступен сценарии дальше выполняться не будут 
   
   Scenario: The user calls the web service to get the site j3
      Given we went to the site
      When the status code is 200
      Then I will check the admin logins and password
      And response includes the following
         | result.msg 		| Welcome!		  |
         | result.complete 	| true            |   
   
  