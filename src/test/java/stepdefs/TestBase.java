/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepdefs;

import babysdays.api.models.login.LoginForm;
import static org.hamcrest.Matchers.equalTo;
import static io.restassured.RestAssured.given;
import org.codehaus.plexus.util.StringUtils;

import java.util.Map;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 *
 * @author sergey
 */
public class TestBase extends BaseClass {

  private final LoginForm loginForm;

  public TestBase() {
    request = given();
    this.loginForm = new LoginForm(section);

  }

  @Given("^we went to the site$")
  public void weWentToTheSite() {
    response = request.when().get(baseHost);
   // System.out.println("response: " + baseHost + response.prettyPrint());
  }

  @When("^the status code is (\\d+)$")
  public void theStatusCodeIs(int statusCode) {
    json = response.then().statusCode(statusCode);
  }

  @Then("^I will check the admin logins and password$")
  public void iWillCheckTheAdminLoginsAndPassword() {
     System.out.println("----=====" + section + "====-------" +  loginForm.getUsername());
    json = given()
            .param("username", loginForm.getUsername())
            .param("password", loginForm.getPassword())
            .when()
            .get(baseHost + '/' + loginForm.getUrlGet())
            .then()
            .statusCode(200);
  }

  @Then("^response includes the following$")
  public void responseIncludesTheFollowing(Map<String, String> responseFields) {
  
    for (Map.Entry<String, String> field : responseFields.entrySet()) {
//      System.out.println(field.getKey() + "====================================== " + response.jsonPath().getString(field.getKey()));
      if (StringUtils.isNumeric(field.getValue())) {
        json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
      } else if (Boolean.valueOf(field.getValue())) {
        json.body(field.getKey(), equalTo(Boolean.parseBoolean(field.getValue())));
      } else {
        json.body(field.getKey(), equalTo(field.getValue()));
      }
    }
   
  }

}
