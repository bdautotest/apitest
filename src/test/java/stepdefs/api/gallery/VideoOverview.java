/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepdefs.api.gallery;

import babysdays.api.models.galleries.VideoChild;
import babysdays.api.models.galleries.VideoMainChildsModels;
import babysdays.api.models.galleries.VideoOptionsModels;
import cucumber.api.java.en.*;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import stepdefs.BaseClass;
import static stepdefs.BaseClass.request;
import static stepdefs.BaseClass.requestSpec;
import static stepdefs.BaseClass.response;
import static com.google.common.truth.Truth.assertThat;
import java.util.List;

/**
 *
 * @author sergey
 */
public class VideoOverview extends BaseClass {

  private final String UrlOptiosGet = "?option=com_sted_mobile&controller=video_gallery&task=ajax_child_start_opt";
  private final String UrlVideoMainsGet = "?option=com_sted_mobile&controller=video_gallery&task=ajax_main";
  public VideoOptionsModels videoOptions;
  public List<VideoChild> videoChildrens;

  @Given("^Got the installation of the system for video$")
  public void gotTheInstallationOfTheSystemForVideo() {
    request = given(requestSpec);
    response = request.when().get(UrlOptiosGet);
  }

  @Then("^check the rest of the video server settings$")
  public void checkTheRestOfTheVideoServerSettings() {
   
    JsonPath jsonVideoOptios = response.jsonPath();

    videoOptions = jsonVideoOptios.getObject("options", VideoOptionsModels.class);
    assertThat(videoOptions.getAid()).isNotNull();
    assertThat(videoOptions.getMaxFileSize()).isNotNull();
    assertThat(videoOptions.getHost()).isNotNull();
      
  }
  
  @Given("^Returns list of children in video gallerie$")
  public void returnsListOfChildrenInVideoGallerie() {
    request = given(requestSpec);
    response = request.when().get(UrlVideoMainsGet);
  }

  @Then("^check the answer of the childrens list for the video section$")
  public void checkTheAnswerOfTheChildrensListForTheVideoSection()  {
      videoChildrens = response.jsonPath()
             .getList("video_children", VideoChild.class);
     }


}
