package stepdefs.api.progress;


import babysdays.api.models.progress.ChildList;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.util.List;
import stepdefs.BaseClass;
import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sergey
 */
public class getChildListProgress extends BaseClass {
  
  /**
   * List of children for the progress section
   * ?option=com_sted_mobile&controller=newprogress&task=getChildList
   */
   public static List<ChildList> progressChildObject;

  String UrlGet = "?option=com_sted_mobile&controller=newprogress&task=getChildList";
  
  @Given("^Make sure there are children for the progress section$")
  public void makeSureThereAreChildrenForTheProgressSection() {
    request = given(requestSpec);
    response = request
            .when()
            .get(UrlGet);

  }

  @When("^Get a list of children for the progress section$")
  public void getAListOfChildrenForTheProgressSection() {
    JsonPath jsonProgressChids = json
            .extract()
            .body()
            .jsonPath();

    progressChildObject = jsonProgressChids.getList("childList", ChildList.class);
    
    if(progressChildObject.size() > 0) {
      idChild =  progressChildObject.get(0).getId();
      assertThat(progressChildObject.get(0).getFirstName()).isNotNull();
      assertThat(progressChildObject.get(0).getLastName()).isNotNull();
      assertThat(progressChildObject.get(0).getPhotograph()).isNotNull();
      assertThat(progressChildObject.get(0).getBirthday()).matches(RegexdateFormaOld);
      assertThat(progressChildObject.get(0).getRoomId()).matches(RegexIsInteger);
      assertThat(progressChildObject.get(0).getRoomTitle()).isNotNull();
      assertThat(progressChildObject.get(0).getAge()).matches(RegexIsInteger);
      assertThat(progressChildObject.get(0).getParents()).matches(RegexIsInteger);
     
    }
    
  }

}
