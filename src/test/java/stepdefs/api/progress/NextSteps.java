/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepdefs.api.progress;

import babysdays.api.models.predefined_room.RoomList;
import babysdays.api.models.progress.NextStep;
import babysdays.api.models.progress.ViewIndividualListModel;
import static com.google.common.truth.Truth.assertThat;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.util.Iterator;
import stepdefs.BaseClass;
import static stepdefs.BaseClass.baseHost;
import static stepdefs.BaseClass.requestSpec;
import static stepdefs.BaseClass.response;
import static stepdefs.api.progress.getChildListProgress.progressChildObject;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sergey
 */
public class NextSteps extends BaseClass {

  private List<NextStep> nextStep;
  private List<ViewIndividualListModel> individualListChildrens;
  private ViewIndividualListModel individual;
  private String childType;
  private String childTab;
  private String childItem;
  private String text;

  public List<NextStep> getNextStep() {
    return nextStep;
  }

  public void setNextStep(List<NextStep> nextStep) {
    this.nextStep = nextStep;
  }

  public String getChildType() {
    return childType;
  }

  public void setChildType(String childType) {
    this.childType = childType;
  }

  public String getChildTab() {
    return childTab;
  }

  public void setChildTab(String childTab) {
    this.childTab = childTab;
  }

  public String getChildItem() {
    return childItem;
  }

  public void setChildItem(String childItem) {
    this.childItem = childItem;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Given("^add notes pending Next Steps of childrens Next Step Text \"([^\"]*)\" Date \"([^\"]*)\"$")
  public void notesPendingNextStepsOfChildrens(String Text, String DateNS, List<NextSteps> arg) {

    String UrlGet = "?option=com_sted_mobile&controller=new_individual&task=create";
    String data = "{"
            + "  \"child_id\":" + idChild + ","
            + "  \"data\": {"
            + "    \"data1\": {";
    for (int i = 0, j = 1; i < arg.size(); i++, j++) {
      data += "      \"eoy" + j + "\": {"
              + "        \"item\": \"" + arg.get(i).getChildItem() + "\","
              + "        \"type\": \"" + arg.get(i).getChildType() + "\""
              + "      },";
    }
    data += "      \"text\": \"" + Text + "\""
            + "    }"
            + "  },"
            + "  \"date\": \"" + DateNS + "\""
            + "}";

    response = given(requestSpec)
            .param("data", data).log().all()
            .when()
            .post(baseHost + '/' + UrlGet);

  }

  @Given("^Shown below are the existing Children on the system$")
  public void shownBelowAreTheExistingChildrenOnTheSystem() {
    String UrlGet = "?option=com_sted_mobile&controller=new_individual&task=individual";
    response = given(requestSpec)
            .when()
            .post(baseHost + '/' + UrlGet);
  }

  @Then("^check existing children for next steps in the system$")
  public void checkExistingChildrenForNextStepsInTheSystem() {
    JsonPath jsonIndividualWiev = response.jsonPath();
    individualListChildrens = jsonIndividualWiev.getList("childrenList", ViewIndividualListModel.class);
    individualListChildrens.get(0);

    for (int i = 0; i < individualListChildrens.size(); i++) {
      individual = individualListChildrens.get(i);
      if (individual.getId() == idChild) {
        break;
      }

    }
    assertThat(individual.getAge()).isNotNull();
    assertThat(individual.getPhotograph()).isNotNull();
    assertThat(individual.getRoomId()).isNotNull();
    assertThat(individual.getRoomTitle()).isNotNull();
    assertThat(individual.getFistName()).isNotNull();
    assertThat(individual.getLastName()).isNotNull();
    assertThat(individual.getBirth()).matches(RegexDateFormaUTF);
    assertThat(individual.isParents()).isFalse();
    assertThat(individual.getAval() > 0 ).isTrue();

    System.err.println("---------------" + individual.getId());

  }

  @Given("^View selected children next steps$")
  public void viewSelectedChildrenNextSteps() {

  }

  private void deleteNotes(String id, String date, String type, String items, String text) {

//    String UrlGet = "?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_del";
//    String encodedText = text;
//    try {
//      encodedText = Base64.getEncoder().encodeToString(text.getBytes("utf-8"));
//    } catch (UnsupportedEncodingException ex) {
//      Logger.getLogger(Observations.class.getName()).log(Level.SEVERE, null, ex);
//    }
//    response = given(requestSpec)
//            .param("chid", chid)
//            .param("date", date)
//            .param("itemid", items)
//            .param("text", encodedText)
//            .param("type", type)
//            .param("type2", 0)
//            .when()
//            .post(baseHost + '/' + UrlGet);
  }
}
