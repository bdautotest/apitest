/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepdefs.api.progress;
import babysdays.api.models.progress.ObsNote;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import stepdefs.BaseClass;
import static stepdefs.BaseClass.requestSpec;
import static stepdefs.BaseClass.response;
import static stepdefs.api.progress.getChildListProgress.progressChildObject;
import java.util.Base64;
import static org.hamcrest.Matchers.hasItems;

/**
 *
 * @author sergey
 */
public class Observations extends BaseClass {

  private String UrlGet = "?option=com_sted_mobile&controller=newprogress&task=obsAddNewprogress";

  OverviewLearningJourney overviewLearningJourney;

  private String chainType;
  private String chainTab;
  private String chainItem;
  private String text;
  private String dateObs;
  private String completed;
  private String type;
  private String textBack;

  public String getTextBack() {
    return textBack;
  }

  public String getDateObsBack() {
    return dateObsBack;
  }
  private String dateObsBack;
  
  
   private List<ObsNote> obsNotes;

  public String getChainType() {
    return chainType;
  }

  public String getChainTab() {
    return chainTab;
  }

  public String getChainItem() {
    return chainItem;
  }

  public String getText() {
    return text;
  }

  public String getDateObs() {
    return dateObs;
  }

  public String getChildId() {
    String childParams = "[" + progressChildObject.get(0).getId() + "]";

    return childParams;
  }

  public String getCompleted() {
    return completed;
  }

  public String getType() {
    return type;
  }

  @Then("^Сlear all notes pending observations$")
  public void сlearAllNotesPendingObservations(DataTable arg) {
    List<List<String>> table = arg.asLists(String.class);
    JsonPath jsonType = response.jsonPath();
    String type, tab, items;

    for (int i = 0; i < table.size(); i++) {

      type = table.get(i).get(0);
      tab = table.get(i).get(1);
      items = table.get(i).get(2);
      
      String typePath = "newprogressList." + type + "." + tab + "." + items;
    
      String jsonObsNotes = jsonType.getString(typePath + ".ObsNotes");

      if (!"".equals(jsonObsNotes)) {
       obsNotes = jsonType.getList(typePath + ".ObsNotes",ObsNote.class);
        for (int j = 0; j < obsNotes.size(); j++) {
          
          deleteNotes (progressChildObject.get(0).getId(),obsNotes.get(j).getDate(),
                  type,items,obsNotes.get(j).getText());
        }
      }
    
    }
  }

  @Given("^notes pending observations of childrens$")
  public void notesPendingObservationsOfChildrens(List<Observations> arg) {
    String notes = "";
    for (int i = 0; i < arg.size(); i++) {
      notes += "__{\"chain_type\":\"" + arg.get(i).getChainType() + "\","
              + "  \"chain_tab\":\"" + arg.get(i).getChainType() + "\","
              + "  \"chain_item\":\"" + arg.get(i).getChainItem() + "\","
              + "  \"text\":\"" + arg.get(i).getText() + "\","
              + "  \"date_obs\":\"" + arg.get(i).getDateObs() + "\","
              + "  \"child_id\":" + getChildId() + ","
              + "  \"completed\":" + arg.get(i).getCompleted() + ","
              + "  \"type\":\"" + arg.get(i).getType() + "\"}";

    }

   response = given(requestSpec)
            .param("aid", getIdAdmin())
            .param("notes", notes)
            .when()
            .post(baseHost + '/' + UrlGet);

  }
  
  @When("^edit notes pending observations of childrens$")
  public void editNotesPendingObservationsOfChildrens(List<Observations> arg)  {
    UrlGet = "?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_edit";
      
      String encodedText     = null,
             encodedtextBack = null;
    try {
      encodedText = Base64.getEncoder().encodeToString(arg.get(0).getText().getBytes("utf-8"))+"\n";
      encodedtextBack = Base64.getEncoder().encodeToString(arg.get(0).getTextBack().getBytes("utf-8"))+"\n";
      
    } catch (UnsupportedEncodingException ex) {
      Logger.getLogger(Observations.class.getName()).log(Level.SEVERE, null, ex);
    }

   response = given(requestSpec)
            .param("chid", idChild)
            .param("date",  arg.get(0).getDateObs())
            .param("date_back",  arg.get(0).getDateObsBack())
            .param("itemid",  arg.get(0).getChainItem())
            .param("text",  encodedText)
            .param("text_back",  encodedtextBack)
            .when()
            .get(baseHost + '/' + UrlGet);
    //System.out.println("---------" + response.prettyPrint());
  }

@Then("^check for \"([^\"]*)\" presence for the child$")
public void checkForPresenceForTheChild(String arg1, DataTable arg) {
    List<List<String>> table = arg.cells(1);
    //  |  cl  | tab1 | CLT97 | test | 17.04.2018| false  | obs  |
    JsonPath jsonType = response.jsonPath();
    String type, tab, items,patch,test,date , arg1title , arg1type;

    for (int i = 0; i < table.size(); i++) {

      type  = table.get(i).get(0);
      tab   = table.get(i).get(1);
      items = table.get(i).get(2);
      test  = table.get(i).get(3).toString();
      date  = table.get(i).get(4);
      arg1title = table.get(i).get(5);
      arg1type = table.get(i).get(6);
      patch = "newprogressList." + type + "." + tab + "." + items+ "."+arg1;
      
      response.then().body(patch+".text", hasItems(test));
      response.then().body(patch+".date", hasItems(date));
      response.then().body(patch+".title", hasItems(arg1title));
      response.then().body(patch+".type", hasItems(arg1type));
     
    }
  }

  @When("^delete for pending presence for the child$")
  public void deleteForPendingPresenceForTheChild(DataTable arg) {
    List<List<String>> table = arg.cells(1);
//   | chainType|  chainItem | text | dateObs   |
    for (int i = 0; i < table.size(); i++) {
          deleteNotes(
               idChild,
               table.get(i).get(3),
               table.get(i).get(0),
               table.get(i).get(1),
               table.get(i).get(2));
    }
  }
  /**
   * 
   * @param chid 
   * @param date
   * @param type
   * @param itemid
   * @param text 
   */
  private void deleteNotes (Integer chid,String date, String type,String itemid,String text) {
     
    UrlGet = "?option=com_sted_mobile&controller=newprogress&task=ajax_progress_notes_del";
    String encodedText = text;
    try {
      encodedText = Base64.getEncoder().encodeToString(text.getBytes("utf-8"));
    } catch (UnsupportedEncodingException ex) {
      Logger.getLogger(Observations.class.getName()).log(Level.SEVERE, null, ex);
    }
    response = given(requestSpec)
            .param("chid",chid)
            .param("date",date)
            .param("itemid",itemid)
            .param("text",encodedText)
            .param("type",type)
            .param("type2",0)
            .when()
            .post(baseHost + '/' + UrlGet);
     
  }

}
