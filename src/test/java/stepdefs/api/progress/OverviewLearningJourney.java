/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepdefs.api.progress;

import babysdays.api.models.progress.ChildList;
import static com.google.common.truth.Truth.assertThat;
import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.codehaus.plexus.util.StringUtils;
import static org.hamcrest.Matchers.*;
import stepdefs.BaseClass;
import static stepdefs.BaseClass.requestSpec;
import static stepdefs.BaseClass.response;
import static stepdefs.api.progress.getChildListProgress.progressChildObject;

/**
 *
 * @author sergey
 */
public class OverviewLearningJourney extends BaseClass {

  String UrlGet = "?option=com_sted_mobile&controller=newprogress&task=getNewprogress";
  /**
   * The first child in the progress section progressChildObject.get(0)
   */
  public ChildList childObject;

  public JsonPath jsonProgressChids;

  /**
   * Get The first child in the progress section progressChildObject.get(0)
   *
   * @return childObject
   */
  public ChildList getChildObject() {
    return childObject;
  }

  /**
   * Get the type of all the progress for the child
   * ?option=com_sted_mobile&controller=newprogress&task=getNewprogress&chid=46&type=cl
   *
   * @return JsonPath
   */
  public JsonPath getJsonProgressChids() {
    return jsonProgressChids;
  }

  String dateAchieved;

  public OverviewLearningJourney() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    this.dateAchieved = dateFormat.format(new Date());
  }

  @Given("^make a request for \"([^\"]*)\" type$")
  public void makeARequestForType(String arg1) {
    childObject = progressChildObject.get(0);
    response = given(requestSpec)
            .param("chid", childObject.getId())
            .param("type", arg1)
            .when().get(UrlGet);
    jsonProgressChids = response
            .body()
            .jsonPath();

  }

  @When("^there must be fields children FirstName for the progress is progress Overview$")
  public void thereMustBeFieldsChildrenFirstNameForTheProgressIsProgressOverview() {

    response.then().assertThat().body("newprogressList.FirstName", equalTo(childObject.getFirstName()));
  }

  @When("^there must be fields children LastName for the progress is progress Overview$")
  public void thereMustBeFieldsChildrenLastNameForTheProgressIsProgressOverview() {
    response.then().assertThat().body("newprogressList.LastName", equalTo(childObject.getLastName()));
  }

  @When("^there must be fields children Age for the progress is progress Overview$")
  public void thereMustBeFieldsChildrenAgeForTheProgressIsProgressOverview() {
    response.then().assertThat().body("newprogressList.Age", equalTo(Integer.parseInt( response.jsonPath().getString("newprogressList.Age"))));
  }

  @When("^there must be fields children DateOfBirth for the progress is progress Overview$")
  public void thereMustBeFieldsChildrenDateOfBirthForTheProgressIsProgressOverview() {
     assertThat( response.jsonPath().getString("newprogressList.DateOfBirth")).matches(RegexdateFormaOld);
//    response.then().assertThat().body("newprogressList.DateOfBirth",  equalTo(childObject.getBirthday()));
  }

  @When("^there must be fields children AssignedRoom for the progress is progress Overview$")
  public void thereMustBeFieldsChildrenAssignedRoomForTheProgressIsProgressOverview() {
    response.then().assertThat().body("newprogressList.AssignedRoom", equalTo(childObject.getRoomTitle()));
  }

  @Given("^make a request for \"([^\"]*)\" type of the child$")
  public void makeARequestForTypeOfTheChild(String type) throws Throwable {
    childObject = progressChildObject.get(0);
    response = given(requestSpec)
            .param("chid", childObject.getId())
            .param("type", type)
            .when().get(UrlGet);
    jsonProgressChids = response
            .body()
            .jsonPath();

  }

  @Given("^clear field if there is a date type \"([^\"]*)\" tab \"([^\"]*)\" itemid \"([^\"]*)\"$")
  public void clearFieldIfThereIsADateTypeTabItemid(String type, String tab, String items) {
    childObject = progressChildObject.get(0);
    String delDateAchieved = response.jsonPath().getString("newprogressList." + type + "." + tab + "." + items + ".DateAchieved");

    if (!StringUtils.isEmpty(delDateAchieved)) {
      response = given(requestSpec)
              .param("type", type)
              .param("tab", tab)
              .param("itemid", items)
              .param("action", 2)
              .param("date", delDateAchieved)
              .param("chid", childObject.getId())
              .when()
              .get("?option=com_sted_mobile&controller=newprogress&task=dateAchievedNewprogress");
    }
  }

  @Given("^add a date achievements today type \"([^\"]*)\" tab \"([^\"]*)\" itemid \"([^\"]*)\"$")
  public void addADateAchievementsTodayTypeTabItemid(String type, String tab, String itemid) {
    childObject = progressChildObject.get(0);
    response = given(requestSpec)
            .param("chid", childObject.getId())
            .param("type", type)
            .param("date", dateAchieved)
            .param("tab", tab)
            .param("itemid", itemid)
            .param("action", 1)
            .when()
            .get("?option=com_sted_mobile&controller=newprogress&task=dateAchievedNewprogress");
  
  }

  @Then("^check the date must \"([^\"]*)\" chievements today type \"([^\"]*)\" tab \"([^\"]*)\" itemid \"([^\"]*)\"$")
  public void checkTheDateMust(String must, String type, String tab, String items) {
    if ("match".equals(must)) {
      response.then().body("newprogressList." + type + "." + tab + "." + items + ".DateAchieved", equalTo(dateAchieved));
    } else {
      response.then().body("newprogressList." + type + "." + tab + "." + items + ".DateAchieved", equalTo(""));
    }
  }

  @Then("^check the parameters for the type progress section$")
  public void checkTheParametersForTheTypeProgressSection(DataTable arg) {
    List<List<String>> table = arg.asLists(String.class);
    JsonPath jsonType = response.jsonPath();
    String type, tab, items, params, val;

    for (int i = 0; i < table.size(); i++) {

      type = table.get(i).get(0);
      tab = table.get(i).get(1);
      items = table.get(i).get(2);
      params = table.get(i).get(3);
      val = table.get(i).get(4);
      response.then().body("newprogressList." + type + "." + tab + "." + items + "." + params, equalTo(val));

    }
  }

  @Given("^delete a date achievements today type \"([^\"]*)\" tab \"([^\"]*)\" itemid \"([^\"]*)\"$")
  public void deleteADateAchievementsTodayTypeTabItemid(String type, String tab, String itemid) {
    childObject = progressChildObject.get(0);
    response = given(requestSpec)
            .param("chid", childObject.getId())
            .param("type", type)
            .param("tab", tab)
            .param("itemid", itemid)
            .param("action", 2)
            .param("date", dateAchieved)
            .when()
            .get("?option=com_sted_mobile&controller=newprogress&task=dateAchievedNewprogress");
  }

  @When("^view the Individual Progress type \"([^\"]*)\"  tab  \"([^\"]*)\"    \"([^\"]*)\" and fields must be \"([^\"]*)\" \"([^\"]*)\"  \"([^\"]*)\"  \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void viewTheIndividualProgressTypeTabsAndFieldsMustBe(
          String type,
          String tab,
          String items,
          String DateAchieved,
          String NSStatus,
          String Video,
          String AlertStatus,
          String StartPoint,
          String Photo,
          String Admin,
          String ObsNotes,
          String NextStep) {

    JsonPath typeJson = response.jsonPath();

    String Patch = "newprogressList";
    String typePatch = Patch + "." + type;
    String tabPatch = typePatch + "." + tab;
    String itemsPatch = tabPatch + "." + items;

    
    response.then().assertThat().body(Patch, hasKey(type));
    response.then().assertThat().body(typePatch, hasKey(tab));
    response.then().assertThat().body(tabPatch, hasKey(items));
    
    writerReqest.append(typeJson.getString(itemsPatch).toString());
    
    typeJson.getMap(itemsPatch).get("ObsNotes");

    response.then().assertThat().body(itemsPatch, hasKey("DateAchieved"));
    response.then().assertThat().body(itemsPatch, hasKey("NSStatus"));
    response.then().assertThat().body(itemsPatch, hasKey("Video"));
    response.then().assertThat().body(itemsPatch, hasKey("AlertStatus"));
    response.then().assertThat().body(itemsPatch, hasKey("StartPoint"));
    response.then().assertThat().body(itemsPatch, hasKey("Photo"));
    response.then().assertThat().body(itemsPatch, hasKey("Admin"));
    response.then().assertThat().body(itemsPatch, hasKey("ObsNotes"));
    response.then().assertThat().body(itemsPatch, hasKey("NextStep"));

  }

}
