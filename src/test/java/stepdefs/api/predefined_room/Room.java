package stepdefs.api.predefined_room;

import babysdays.api.models.predefined_room.RoomList;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.io.IOException;
import stepdefs.BaseClass;

/**
 *
 * @author sergey
 */
public class Room extends BaseClass {

  String UrlGet = "?option=com_sted_mobile&controller=predefined_room&task=room&limitstart_=0";

  @Given("^Make sure there are rooms$")
  public void makeSureThereAreRooms() throws IOException {
    request = given(requestSpec);
    response = request.when().get(UrlGet);
  }

  @When("^get list of all rooms$")
  public void getListOfAllRooms() {
    JsonPath jsonRoom = json
            .extract()
            .body()
            .jsonPath();

    roomObject = jsonRoom.getList("roomList", RoomList.class);
  
  }

}
