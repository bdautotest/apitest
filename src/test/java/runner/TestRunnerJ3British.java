package runner;

import babysdays.api.models.login.LoginForm;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import stepdefs.BaseClass;
import utilities.DefaultParams;

/**
 * Created by Amit Rawat on 3/29/2016.
 */

@CucumberOptions(
        monochrome = true,
        features = "src/test/resources/features/",
        tags = {"@API", "@FunctionTests", "@J3" ,"@british"},
        glue = {"stepdefs"},
        format = {
          "pretty",
          "html:target/cucumber-report/html",
          "json:target/cucumber-report/cucumber.json",
          "rerun:target/cucumber-report/rerun.txt"
        })

public class TestRunnerJ3British {

  private TestNGCucumberRunner testNGCucumberRunner;

  private final String section = "j3_british";

  @BeforeClass(alwaysRun = true)
  public void setUpClass() throws Exception {

    BaseClass.setSection(section);
    BaseClass.setLoginForm(new LoginForm(section));
    BaseClass.setBaseHost(DefaultParams.loadData(section, "BseURL"));

    testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

  }

  @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
  public void feature(CucumberFeatureWrapper cucumberFeature) {
    testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
  }

  @DataProvider
  public Object[][] features() {
    return testNGCucumberRunner.provideFeatures();
  }

  @AfterClass(alwaysRun = true)
  public void tearDownClass() throws Exception {
    testNGCucumberRunner.finish();
  }
}
