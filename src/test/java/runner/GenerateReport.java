package runner;

import net.masterthought.cucumber.ReportBuilder;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.masterthought.cucumber.Configuration;

/**
 * Created by Sergey
 */
public class GenerateReport {
  

  public static void GenerateMasterthoughtReport() {
    File reportOutputDirectory = new File("target");
    List<String> jsonFiles = new ArrayList<>();
    jsonFiles.add("target/cucumber15.json");
    jsonFiles.add("target/cucumber3.json");

    String jenkinsBasePath = "";
    String buildNumber = "1";
    String projectName = "babysdays api";
//    boolean skippedFails = true;
//    boolean pendingFails = false;
//    boolean undefinedFails = true;
//    boolean missingFails = true;
//
    boolean parallelTesting = true;
    boolean runWithJenkins = false;

    Configuration configuration = new Configuration(reportOutputDirectory, projectName);
    configuration.setParallelTesting(parallelTesting);
    configuration.setBuildNumber(buildNumber);
    configuration.setRunWithJenkins(runWithJenkins);

    ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
    reportBuilder.generateReports();
  }

  /**
   * 
   * @param section J3 | J15
   */
  static void GenerateMasterthoughtReport(String section) {
    File reportOutputDirectory = new File("target/"+section);
  
    List<String> jsonFiles = new ArrayList<>();
    jsonFiles.add("target/cucumber"+section+".json");
   
    String jenkinsBasePath = "";
    String buildNumber = "1";
    String projectName = "babysdays api " + section;

    boolean parallelTesting = true;
    boolean runWithJenkins = false;

    Configuration configuration = new Configuration(reportOutputDirectory, projectName);
    configuration.setParallelTesting(parallelTesting);
    configuration.setBuildNumber(buildNumber);
    configuration.setRunWithJenkins(runWithJenkins);

    ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
    reportBuilder.generateReports();
  }
}
