/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package runner;

import org.testng.IExecutionListener;

/**
 *
 * @author sergey
 */
public class TestNGExecutionListenerj3British implements IExecutionListener {
    @Override
    public void onExecutionStart() {
//        System.out.println("TestNG is staring the execution");
    }
    @Override
    public void onExecutionFinish() {
        System.out.println("Generating the Masterthought Report j3 British");
        GenerateReport.GenerateMasterthoughtReport("j3_british");
        System.out.println("TestNG has finished, the execution j3 British");
    }
}
