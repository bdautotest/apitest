package runner;

import babysdays.api.models.login.LoginForm;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import stepdefs.BaseClass;
import utilities.DefaultParams;

/**
 * Created by Amit Rawat on 3/29/2016.
 */
@CucumberOptions(
        glue = "stepdefs",
        features = "src/test/resources/features/",
        tags = {"@API", "@FunctionTests", "@J3","@scottish"},
        plugin = {
          "pretty",
          "html:target/j3_scottish/html-report",
          "json:target/cucumberj3_scottish.json"
        },
        snippets = SnippetType.CAMELCASE
)

public class TestRunnerJ3Scottish {

  private TestNGCucumberRunner testNGCucumberRunner;

  private final String section = "j3_scottish";

  @BeforeClass(alwaysRun = true)
  public void setUpClass() throws Exception {

    BaseClass.setSection(section);
    BaseClass.setLoginForm(new LoginForm(section));
    BaseClass.setBaseHost(DefaultParams.loadData(section, "BseURL"));

    testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

  }

  @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
  public void feature(CucumberFeatureWrapper cucumberFeature) {
    testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
  }

  @DataProvider
  public Object[][] features() {
    return testNGCucumberRunner.provideFeatures();
  }

  @AfterClass(alwaysRun = true)
  public void tearDownClass() throws Exception {
    testNGCucumberRunner.finish();
  }
}
