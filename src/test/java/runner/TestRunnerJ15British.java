package runner;

import babysdays.api.models.login.LoginForm;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.testng.CucumberFeatureWrapper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import stepdefs.BaseClass;

import utilities.DefaultParams;

@CucumberOptions(
        glue = "stepdefs",
        features = "src/test/resources/features/",
        tags = {"@API", "@FunctionTests", "@J15" ,"@british"},
        plugin = {
          "pretty",
          "html:target/j15_british/html-report",
          "json:target/cucumberj15_british.json"
        },
        snippets = SnippetType.CAMELCASE
)

public class TestRunnerJ15British {

  private TestNGCucumberRunner testNGCucumberRunner;

  private final String section = "j15_british";

  @BeforeClass(alwaysRun = true)
  public void setUpClass() throws Exception {
    BaseClass.setSection(section);
    BaseClass.setLoginForm(new LoginForm(section));
    BaseClass.setBaseHost(DefaultParams.loadData(section, "BseURL"));

    
    testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

  }

  @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
  public void feature(CucumberFeatureWrapper cucumberFeature) {
    testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
  }

  @DataProvider
  public Object[][] features() {
    return testNGCucumberRunner.provideFeatures();
  }

  @AfterClass(alwaysRun = true)
  public void tearDownClass() throws Exception {
    testNGCucumberRunner.finish();
  }
}
